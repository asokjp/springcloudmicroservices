# Copyright 2017 Istio Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
##################################################################################################
# helloworld service
##################################################################################################
apiVersion: v1
kind: Service
metadata:
  name: helloworld
  labels:
    app: helloworld
spec:
  ports:
  - port: 8081
    name: http
  selector:
    app: helloworld
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  annotations:
    sidecar.istio.io/status: injected-version-root@822a7ac3ca86-0.2.4-9c7c291eab0a522f8033decd0f5b031f5ed0e126
  creationTimestamp: null
  name: helloworld-v1
spec:
  replicas: 1
  strategy: {}
  template:
    metadata:
      annotations:
        sidecar.istio.io/status: injected-version-root@822a7ac3ca86-0.2.4-9c7c291eab0a522f8033decd0f5b031f5ed0e126
      creationTimestamp: null
      labels:
        app: helloworld
        version: v1
    spec:
      containers:
      - env:
        - name: SPRING_PROFILES_ACTIVE
          value: dev
        image: gcr.io/adroit-flare-179614/hello-world-v1
        imagePullPolicy: IfNotPresent
        name: helloworld
        ports:
        - containerPort: 8081
        resources: {}
      - args:
        - proxy
        - sidecar
        - -v
        - "2"
        - --configPath
        - /etc/istio/proxy
        - --binaryPath
        - /usr/local/bin/envoy
        - --serviceCluster
        - istio-proxy
        - --drainDuration
        - 45s
        - --parentShutdownDuration
        - 1m0s
        - --discoveryAddress
        - istio-pilot.istio-system:8080
        - --discoveryRefreshDelay
        - 30s
        - --zipkinAddress
        - zipkin.istio-system:9411
        - --connectTimeout
        - 10s
        - --statsdUdpAddress
        - istio-mixer.istio-system:9125
        - --proxyAdminPort
        - "15000"
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: INSTANCE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        image: docker.io/istio/proxy_debug:0.2.4
        imagePullPolicy: IfNotPresent
        name: istio-proxy
        resources: {}
        securityContext:
          privileged: true
          readOnlyRootFilesystem: false
          runAsUser: 1337
        volumeMounts:
        - mountPath: /etc/istio/proxy
          name: istio-envoy
        - mountPath: /etc/certs/
          name: istio-certs
          readOnly: true
      initContainers:
      - args:
        - -p
        - "15001"
        - -u
        - "1337"
        - -i
        - 10.36.0.0/14,10.39.240.0/20
        image: docker.io/istio/proxy_init:0.2.4
        imagePullPolicy: IfNotPresent
        name: istio-init
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
          privileged: true
      - args:
        - -c
        - sysctl -w kernel.core_pattern=/etc/istio/proxy/core.%e.%p.%t && ulimit -c
          unlimited
        command:
        - /bin/sh
        image: alpine
        imagePullPolicy: IfNotPresent
        name: enable-core-dump
        resources: {}
        securityContext:
          privileged: true
      volumes:
      - emptyDir:
          medium: Memory
          sizeLimit: "0"
        name: istio-envoy
      - name: istio-certs
        secret:
          optional: true
          secretName: istio.default
status: {}
---
##################################################################################################
# claimant service
##################################################################################################
apiVersion: v1
kind: Service
metadata:
  name: claimantservice
  labels:
    app: claimantservice
spec:
  ports:
  - port: 8082
    name: http
  selector:
    app: claimantservice
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  annotations:
    sidecar.istio.io/status: injected-version-root@822a7ac3ca86-0.2.4-9c7c291eab0a522f8033decd0f5b031f5ed0e126
  creationTimestamp: null
  name: claimantservice-v1
spec:
  replicas: 1
  strategy: {}
  template:
    metadata:
      annotations:
        sidecar.istio.io/status: injected-version-root@822a7ac3ca86-0.2.4-9c7c291eab0a522f8033decd0f5b031f5ed0e126
      creationTimestamp: null
      labels:
        app: claimantservice
        version: v1
    spec:
      containers:
      - env:
        - name: SPRING_PROFILES_ACTIVE
          value: dev
        image: gcr.io/adroit-flare-179614/claimant-service-v1
        imagePullPolicy: IfNotPresent
        name: claimantservice
        ports:
        - containerPort: 8082
        resources: {}
        volumeMounts:
        - mountPath: /etc/mongo/config
          name: mongoconfig
      - args:
        - proxy
        - sidecar
        - -v
        - "2"
        - --configPath
        - /etc/istio/proxy
        - --binaryPath
        - /usr/local/bin/envoy
        - --serviceCluster
        - istio-proxy
        - --drainDuration
        - 45s
        - --parentShutdownDuration
        - 1m0s
        - --discoveryAddress
        - istio-pilot.istio-system:8080
        - --discoveryRefreshDelay
        - 30s
        - --zipkinAddress
        - zipkin.istio-system:9411
        - --connectTimeout
        - 10s
        - --statsdUdpAddress
        - istio-mixer.istio-system:9125
        - --proxyAdminPort
        - "15000"
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: INSTANCE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        image: docker.io/istio/proxy_debug:0.2.4
        imagePullPolicy: IfNotPresent
        name: istio-proxy
        resources: {}
        securityContext:
          privileged: true
          readOnlyRootFilesystem: false
          runAsUser: 1337
        volumeMounts:
        - mountPath: /etc/istio/proxy
          name: istio-envoy
        - mountPath: /etc/certs/
          name: istio-certs
          readOnly: true
      initContainers:
      - args:
        - -p
        - "15001"
        - -u
        - "1337"
        - -i
        - 10.36.0.0/14,10.39.240.0/20
        image: docker.io/istio/proxy_init:0.2.4
        imagePullPolicy: IfNotPresent
        name: istio-init
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
          privileged: true
      - args:
        - -c
        - sysctl -w kernel.core_pattern=/etc/istio/proxy/core.%e.%p.%t && ulimit -c
          unlimited
        command:
        - /bin/sh
        image: alpine
        imagePullPolicy: IfNotPresent
        name: enable-core-dump
        resources: {}
        securityContext:
          privileged: true
      volumes:
      - name: mongoconfig
        secret:
          secretName: mongosecret
      - emptyDir:
          medium: Memory
          sizeLimit: "0"
        name: istio-envoy
      - name: istio-certs
        secret:
          optional: true
          secretName: istio.default
status: {}
---
###########################################################################
# Ingress resource (gateway)
##########################################################################
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: gateway
  annotations:
    kubernetes.io/ingress.class: "istio"
spec:
  rules:
  - http:
      paths:
      - path: /helloWorld/*
        backend:
          serviceName: helloworld
          servicePort: 8081
      - path: /helloWorld/.*
        backend:
          serviceName: helloworld
          servicePort: 8081
      - path: /message1
        backend:
          serviceName: helloworld
          servicePort: 8081
      - path: /claimant
        backend:
          serviceName: claimantservice
          servicePort: 8082
      - path: /message
        backend:
          serviceName: claimantservice
          servicePort: 8082    
---
